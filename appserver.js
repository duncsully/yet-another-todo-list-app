const express = require('express'),
    app = express(),
    path = require('path'),
    PORT = 9001;

let todoItems = [],
    count = 0;

app.use(express.static(path.resolve(__dirname, './build')));

// Just direct every request to index
app.get('/*', function(req, res) {
    res.sendFile(path.resolve(__dirname, './build/index.html'));
});

// Setup WebSocket capability
const server = app.listen(9001),
    io = require('socket.io')(server);

/**
 * Sends all socket connections the latest list
 */
function updateClients() {
    io.emit('update', todoItems);
}

/**
 * Add a new item to the list
 * @param {string} val - Text to display for list item
 */
function addItem(val) {
    const newItem = {
        text: val,
        id: Date.now() + '-' + count++,
        created: Date.now(),
        done: false
    };
    todoItems.push(newItem);
    console.log(`Adding item: "${val}" with ID: ${newItem.id}`);
    updateClients();
}

/**
 * Delete an item from list
 * @param {string} id - ID of item to delete
 */
function deleteItem(id) {
    todoItems = todoItems.filter(item => item.id !== id);
    console.log('Deleting item: ' + id);
    updateClients();
}

/**
 * Sets an item to done, setting the date-time of completion
 * @param {string} id - ID of item to mark done
 */
function setItemDone(id) {
    const item = getItemByID(id);
    item.done = Date.now();
    console.log(`Setting done for item: ${id}`);
    updateClients();
}

function setItemUndone(id) {
    const item = getItemByID(id);
    item.done = false;
    console.log(`Setting undone for item: ${id}`);
    updateClients();
}


function getItemByID(id) {
    return todoItems.find(item => item.id === id);
}

// Setup all socket handlers
io.on('connection', function(socket) {
    console.log('New connection: ' + socket.request.connection.remoteAddress);
    socket.on('addItem', addItem);
    socket.on('deleteItem', deleteItem);
    socket.on('setItemDone', setItemDone);
    socket.on('setItemUndone', setItemUndone);
    // Gives client current list when they connect
    socket.emit('update', todoItems);
});

console.log('Todo list running at http://localhost:' + PORT);
