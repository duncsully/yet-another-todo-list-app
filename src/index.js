import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import io from '../node_modules/socket.io-client/dist/socket.io.js';

// Setup WebSocket connection to server (explicitly defined for dev since not on same port)
const host = process.env.NODE_ENV === 'development' ? window.location.protocol + '//' + window.location.hostname + ':9001' : null,
socket = io(host);

ReactDOM.render(<App socket={socket}/>, document.getElementById('root'));
