import React, {useState} from 'react';

/**
 * To-do list React component
 * @param {array} items - The list items to render
 *  @param {string} items[].id - ID of item
 *  @param {string} items[].text - Text to render
 *  @param {boolean} items[].done - If item is complete or not
 * @param {function} onNewItem - Handler for new item, gets passed item text
 * @param {function} onDelete - Handler for deleting item, gets passed item ID
 * @param {function} onSetDone - Handler for setting completion state, gets passed item ID and done status
 * @returns {JSX}
 * @constructor
 */
function TodoList({ items, onNewItem, onDelete, onSetDone }) {
    // TODO: Default handlers;
    const [newItemVal, setNewItemVal] = useState('');

    /**
     * Call passed in new item handler on enter
     * @param {event} e - Event object
     */
    function handleKeyPress(e) {
        if (e.key === 'Enter')  {
            onNewItem(e.target.value);
            // Blank out new item input
            setNewItemVal('');
        }
    }

    // TODO: Allow editing and reorder?
    // Separate items into to-do and done lists
    const {todoItems, doneItems} = items.reduce((results, item) => {
        const arrToPush = item.done ? 'doneItems' : 'todoItems';
        results[arrToPush].push(
            <li className="listItem" key={item.id}>
                <input type="checkbox" checked={item.done} onChange={() => onSetDone(item.id, !item.done)}/>
                <span className={item.done ? "strikethrough" : ""}>{item.text}</span>
                <button className="deleteItem" title="Delete" onClick={() => onDelete(item.id)}>X</button>
            </li>
        );
        return results;
    }, {todoItems: [], doneItems: []});

    // Add new item field
    todoItems.push(
        <li key="newItem">
            <span className="newItemIcon">+</span>
            <input className="newItem" placeholder="New item" value={newItemVal} onKeyPress={handleKeyPress} onChange={(e) => setNewItemVal(e.target.value)}/>
        </li>
    );

    return (
        <div className="listContainer">
            <h3>Todo:</h3>
            <ul className="list">{todoItems}</ul>
            <h3>Done:</h3>
            {doneItems.length ? <ul className="list">{doneItems}</ul> : <p>Nothing yet</p>}
        </div>
    )
}

export default TodoList;
