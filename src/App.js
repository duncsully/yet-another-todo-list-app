import React, {useState, useEffect} from 'react';
import './App.css';
import TodoList from "./components/TodoList";

function App({ socket }) {
    const [listItems, setListItems] = useState([]),
        [error, setError] = useState('');


    useEffect(() => {
        // Update from the server
        socket.on('update', newList => {
            setListItems(newList);
            setError('');
        });

        return () => socket.removeAllListeners('update');
    });


    useEffect(() => {
        // No connection to server or connection lost
        socket.on('connect_error', () => setError('No connection to the server, try again later'));

        return () => socket.removeAllListeners('connect_error');
    });

    /**
     * Messages server new item
     * @param {string} val - Text for new item
     */
    function handleNewItem(val) {
        if (val !== '') socket.emit('addItem', val);
    }

    /**
     * Messages server item to delete
     * @param {string} id - ID of item to delete
     */
    function handleDelete(id) {
        socket.emit('deleteItem', id);
    }

    /**
     * Message server item to toggle completion of
     * @param {string} id - ID of item to toggle
     */
    function handleSetDone(id, done) {
        socket.emit(done ? 'setItemDone' : 'setItemUndone', id);
    }

    return (
        <div className="App">
            <div className="content">
                <h1>Yet Another Todo List App</h1>
                {error
                    ? <p>{error}</p>
                    : <TodoList items={listItems} onNewItem={handleNewItem} onDelete={handleDelete}
                                onSetDone={handleSetDone}/>
                }
            </div>
        </div>
    );
}

export default App;
